RESTAuth Module
------------
by Klaus Purer, klausi@fsinf.at

The RESTAuth module integrates the Drupal user login and registration with a
RESTAuth server that authenticates users and their passwords. The RestAuth
project is a system providing shared authentication. In order to use this module
you need to setup a RESTAuth server, read the documentation:

http://restauth.net


Installation
-------------
 * Copy the whole restauth directory to your modules directory
   (e.g. DRUPAL_ROOT/sites/all/modules) 
 * Checkout the RESTAuth PHP library:

   cd DRUPAL_ROOT/sites/all/libraries
   git clone git://git.fsinf.at/restauth/php.git restauth_php

 * Activate the RESTAuth module.
 * Go to the URL 'admin/config/services/restauth' and fill out the credentials for
   your RESTAuth server.
